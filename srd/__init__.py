#!/usr/bin/env python
# coding: utf-8
#      __init__.py
"""
Implementation of request classes
"""

import requests
try:
    import torrequest
    TOR = True
except ImportError:
    TOR = False


class TorRequest():
    """
    Requests using torrequest module
    """
    def __init__(self):
        self.__req = None

    def do_request(self, link: str, params: tuple = None,
                   verify: bool = False) -> None:
        """Performs request"""
        if params is None:
            params = ()
        try:
            with torrequest.TorRequest() as treq:
                self.__req = treq.get(link, verify=verify)
        except Exception as err:
            print(str(err))

    def content(self) -> str:
        """Returns reply of a request"""
        return self.__req.text


class Request():
    """
    Requests using standard requests library
    """
    def __init__(self):
        self.__req = None

    def do_request(self, link: str, params: tuple = None,
                   verify: bool = False) -> None:
        """Performs request"""
        try:
            self.__req = requests.get(link, params, verify=verify)
        except Exception as err:
            print(str(err))

    def content(self) -> str:
        """Returns reply of a request"""
        return self.__req.text
