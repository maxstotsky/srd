#!/usr/bin/env python
# coding: utf-8
#      srd.py
"""
Simple script to make request to several sites and services
"""

import re
import sys
import time
import multiprocessing

from srd import TorRequest, Request, TOR

ADDITIONAL_LINKS = []
SRD_LINK = "https://stop-russian-desinformation.near.page/"
DEBUG = True
NUM_OF_REQUESTS_PER_SITE = 3
PROC_TIMEOUT = 3


def srd_parse(html: str) -> list:
    """
    Parse html page to make http link list
    """
    links = []
    for line in html.split("\n"):
        mtch = re.match("  '(https?://.*/)',", line)
        if mtch:
            links.append(mtch.group(1))
    return links


def main(argv: list) -> None:
    """
    Entry point
    """
    srd = Request()
    srd.do_request(SRD_LINK, verify=True)
    links = srd_parse(srd.content())
    links += ADDITIONAL_LINKS
    if DEBUG:
        print("\n".join(links))
    requests_pool = {}
    while True:
        try:
            for link in links:
                if link not in requests_pool:
                    requests_pool[link] = []
                while len(requests_pool[link]) < NUM_OF_REQUESTS_PER_SITE:
                    reqcls = TorRequest if TOR else Request
                    req = reqcls()
                    prc = multiprocessing.Process(
                        target=reqcls.do_request, args=(req, link,))
                    prc.start()
                    requests_pool[link].append((prc, time.time()))
                    if DEBUG:
                        print("{0}.do_request => {1}".format(
                            reqcls.__name__, link))
                new_pool = []
                for prc, stime in requests_pool[link]:
                    if time.time() - stime > PROC_TIMEOUT:
                        prc.kill()
                        prc.join()

                    if prc.is_alive():
                        new_pool.append((prc, stime))
                    else:
                        prc.join()
                requests_pool[link] = new_pool
            time.sleep(1)
        except Exception as err:
            print("=" * 80)
            print(str(err))
            print("=" * 80)
            time.sleep(5)


if __name__ == "__main__":
    main(sys.argv)
